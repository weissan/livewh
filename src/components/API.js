import React, { Component } from 'react';
import { Alert } from 'react-native';
import Path from 'react-native-path';
import QueryString from 'query-string';
import Sound from 'react-native-sound'; //playAudio
import {ProgressBar} from '../CrossPlatform';
import {AudioRecorder, AudioUtils} from 'react-native-audio';

export const submitLectureMessageJson = function(data) {
  return fetch('https://live.curries.cc/api/v1/lectures/'+this.id+'/messages', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '+this.props.user.token,
    },
    body: QueryString.stringify(data),
  }).then( response=> {
    if (response.ok) {
      return Promise.resolve(response)
    } else {
      return Promise.reject(response.status)
    }
  }).then( response=>{
    return response.json()
  })
}

export const submitLectureMessageFormData = function(formData) {
  return fetch('https://live.curries.cc/api/v1/lectures/'+this.id+'/messages', {
    method: 'POST',
    headers: {
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer '+this.props.user.token,
    },
    body: formData,
  }).then( response=> {
    if (response.ok) {
      return Promise.resolve(response)
    } else {
      return Promise.reject(response.status)
    }
  }).then(response=>{
    return response.json()
  })
}

export const getLectureMessageList = function() {
  return fetch('https://live.curries.cc/api/v1/lectures/'+this.id+'/messages?offset='+this.state.messageList.length+'&count='+this.count+'&from=1', {
    method: 'GET',
    headers: {
      'Authorization': 'Bearer '+this.props.user.token,
    },
  }).then( response=> {
    if (response.ok) {
      return Promise.resolve(response)
    } else {
      return Promise.reject(response.status)
    }
  }).then( response=>{
    return response.json()
  }).then( messageList=>{
    if (messageList.length > 0) {
      messageList.forEach((item,i) => {
        switch(item.type) {
          case 1:
          case 2:
          case 3:
          case 4:
            this.classifiedMessageList[item.type].push(item.content);
            break;
          default:
            return Promise.reject('illegal message:'+JSON.Stringify(item))
        }
      });
      return Promise.resolve(this.state.messageList.concat(messageList))
    } else {
      return Promise.reject('No more message')
    }
  })
}

export const getLectureGuide = function() {
  return fetch('https://live.curries.cc/api/v1/lectures/'+this.id+'?guide=1'
  ).then(response=> {
    if (response.ok) {
      return Promise.resolve(response)
    } else {
      return Promise.reject(response.status)
    }
  }).then(response=> {
    return response.json()
  }).then(result=>{
    return Promise.resolve(result.guide)
  })
}

export const getLectureApplying = function() {
  return fetch('https://live.curries.cc/api/v1/lectures/'+this.id+'/applying', {
    method: 'GET',
    headers: {
      'Authorization': 'Bearer '+this.props.user.token,
    }
  }).then(response=> {
    if (response.ok) {
      return Promise.resolve(response)
    } else {
      return Promise.reject(response.status)
    }
  }).then(response=> {
    return response.json()
  })
}

export const submitLectureApplying = function() {
  return fetch('https://live.curries.cc/api/v1/lectures/'+this.id+'/applying', {
    method: 'POST',
    headers: {
      'content-type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer '+this.props.user.token,
    }
  }).then(response=> {
    if (response.ok) {
      return Promise.resolve(response)
    } else {
      return Promise.reject(response.status)
    }
  }).then(response=> {
    return response.json()
  })
}

export const getLectureList = function() {
  return fetch('https://live.curries.cc/api/v1/lectures?offset='+this.state.lectureList.length+'&count='+this.count
  ).then(response=> {
    if (response.ok) {
      return Promise.resolve(response)
    } else {
      return Promise.reject(response.status)
    }
  }).then(response=> {
    return response.json()
  }).then(lectureList => {
    if (lectureList.length > 0) {
      return Promise.resolve(this.state.lectureList.concat(lectureList))
    } else {
      return Promise.reject('No more lecture')
    }
  })
}

// currently, http server only handle one file in one formdata
export const uploadFile = function(response) {
  if (response.didCancel) {
    return Promise.reject('didCancel')
  }
  else if (response.error) {
    return Promise.reject(error)
  }
  else {
    const formData = new FormData();
    let type
    switch(Path.dirname(response.mime)) {
      case "image":
        type=2
        break
      case "audio":
        type=3
        break
      case "video":
        type=4
        break
      default:
        return Promise.reject('illegal mime:'+response.mime)
    }
    formData.append('type', type);
    formData.append('content', '');
    formData.append('uploadfile', {
      uri:response.path,
      type:response.mime,
      name:Path.basename(response.path),
    });
    return submitLectureMessageFormData.call(this,formData)
  }
}

const playAudio = function(msg) {
  msg.audioProgressIntervalID = setInterval(() => {
    msg.progress += 0.5/msg.duration;
    msg.progressBar.setNativeProps({
      progress:msg.progress
    });
  },500);
  msg.audioPlayer.play(() => {
    stopAudio.call(this,msg);
  });
}

const pauseAudio = function(msg){
  clearInterval(msg.audioProgressIntervalID)
  msg.audioPlayer.pause();
}

export const stopAudio = function(msg){
  pauseAudio.call(this,msg);
  msg.progress = 1
  msg.progressBar.setNativeProps({
    progress:msg.progress,
  });
  msg.audioPlayer.release();
  this.audioIndex = null;
}

export const startRecordAudio = function(){
  if (!this.recordPath) {
    this.recordPath=Math.floor(Date.now() / 1000)

    AudioRecorder.prepareRecordingAtPath(AudioUtils.DocumentDirectoryPath + '/' + this.recordPath+ '.aac',{
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: "Low",
      AudioEncoding: "aac",
      AudioEncodingBitRate: 32000
    });

    AudioRecorder.onProgress = (data) => {
      this.recordProgressBar.setNativeProps({progress: ((Math.floor(Date.now()/1000))-this.recordPath)/60});
    };

    AudioRecorder.startRecording()
  } else {
    AudioRecorder.stopRecording().then(filePath=>{
      Alert.alert(
        'Info',
        'Sure to send?',
        [
          {
            text:'OK',
            onPress: () => {
              uploadFile.call(this,{path:'file://'+filePath, mime:'audio/aac'}
                ).catch(e=>{
                  Alert.alert('Failure',JSON.stringify(e),[ {text:'OK'}],{cancelable:false})
                }).finally(()=>{
                  this.recordProgressBar.setNativeProps({progress:0})
                })
              }
          }, {
            text:'Cancel',
            onPress:()=>
              this.recordProgressBar.setNativeProps({progress:0})
          },
        ],
        {cancelable:false}
      )
    })
    this.recordPath=null
  }
}

export const startPlayAudio = function(index){
  if (null !== this.audioIndex && index !== this.audioIndex) {
    let msg = this.state.messageList[this.audioIndex]
    stopAudio.call(this,msg);
  }
  let msg = this.state.messageList[index]
  if (null === this.audioIndex) {
    this.audioIndex = index
    msg.progress = 0
    msg.audioPlayer = new Sound(msg.content, undefined, (e,props) => {
      if (e) {
        console.log('failed to load the sound', e);
      } else {
        playAudio.call(this,msg);
      }
    });
  } else {
    msg.audioPlayer.getCurrentTime( (seconds, isPlaying) => {
      if (isPlaying) {
        pauseAudio.call(this,msg);
      } else {
        playAudio.call(this,msg);
      }
    });
  }
}
