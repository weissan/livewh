/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
 "use strict";

import React, { Component } from 'react';
import { Button, Text, View } from 'react-native';
import { connect } from 'react-redux';

class Home extends React.Component {
  static navigationOptions = {
    title: 'Home',
    header: null,
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{flex:1, flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
        <Button
          title="Go to Live"
          onPress={() => navigate('LectureRoom',{id:137})}
        />
      </View>
    );
  }
}

export {Home as default};
