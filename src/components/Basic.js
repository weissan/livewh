"user strict";

import React, { Component } from 'react';
import {TouchableOpacity, StyleSheet, Text, Image} from 'react-native';

export function TextButton(props) {
  return (
    <TouchableOpacity style={styles.buttonContainer}
      onPress={props.onPress}>
      <Text style={styles.buttonText}>{props.title}</Text>
    </TouchableOpacity>
  );
}

export function ImageButton(props) {
  return (
    <TouchableOpacity style={styles.buttonContainer}
      onPress={props.onPress}>
      <Image source={props.source} />
    </TouchableOpacity>
  );
}


export const styles = StyleSheet.create({
  container: {
   flex: 1,
  },

  buttonContainer:{
    padding: 15
  },

  buttonText:{
    textAlign: 'center',
    fontWeight: '700'
  },
});
