 "use strict";

import React, { Component } from 'react';
import { View, Text, FlatList, Image, ImageBackground, Dimensions, TouchableOpacity, Alert, TouchableWithoutFeedback, AppState, KeyboardAvoidingView, TextInput, Modal} from 'react-native';
import { connect } from 'react-redux';
import * as Basic from './Basic';
import * as API from './API';
import ImagePicker from 'react-native-image-crop-picker';
import {ProgressBar} from '../CrossPlatform';
import Icon from 'react-native-vector-icons/FontAwesome';
const { width, height } = Dimensions.get('window');

class LectureRoom extends React.Component {
  static navigationOptions = {
    title: 'Room',
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      messageList : [],
      modalVisible: false,
      textInputVisible:true,
    };
    this.id = props.navigation.state.params.id;
    this.audioIndex = null;
    this.recordPath = null;
    this.classifiedMessageList = [null,[],[],[],[]];
    this.count=10
    this.ws = new WebSocket('wss://live-ws.curries.cc/ws/lectures/'+this.id);
  }

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);

    this.ws.onopen = () => {
      console.log('ws openned');
    };

    this.ws.onmessage = (e) => {
      try {
        let message = JSON.parse(e.data)
        this.classifiedMessageList[message.type].unshift(message.content);
        this.setState(previousState=>
          ({messageList:(new Array(message)).concat(previousState.messageList)})
        );
      } catch(error) {
      }
    };

    this.ws.onerror = (e) => {
      console.log('onerror:');
    };

    this.ws.onclose = (e) => {
      console.log('onclose:');
    };
  }

  render() {
    return (
      <View style={Basic.styles.container}>
        <Basic.TextButton
          onPress={()=>{
            API.getLectureMessageList.call(this).then( messageList=>{
              this.setState({messageList:messageList})
            }).catch( e=>{
              Alert.alert('Failure',JSON.stringify(e),[ {text:'OK',},],{cancelable:false})
            });
          }}
          title={"Click to get " + this.count + " more..."}
        />
        <FlatList
          data={this.state.messageList}
          renderItem={this._renderItem}
          removeClippedSubviews={true}
          keyExtractor= {(item, i) => i}
          inverted={true}
        />
        <View style={{flexDirection:'row'}}>
          <Icon.Button name="microphone" size={width * 0.1}
            onPress={ () =>
              this.setState(previousState=>({textInputVisible:!previousState.textInputVisible}))
            }>
          </Icon.Button>
          {this.state.textInputVisible?
            <TextInput
              ref={ref=>this._textInput=ref}
              style={{
                flex:1,
              }}
              onSubmitEditing={(event) => {
                API.submitLectureMessageJson.call(this,{
                  type:1,
                  content: event.nativeEvent.text,
                }).then( r=>{
                  this._textInput.clear()
                }).catch( e=>{
                  Alert.alert('Failure',JSON.stringify(e),[ {text:'OK',},],{cancelable:false})
                });
              }}
              autoCapitalize={'none'}
              autoCorrect={false}
              returnKeyType ={'send'} //ios
              returnKeyLabel = {'send'} //android
            />
          :
          <TouchableWithoutFeedback
            onPress={()=>{API.startRecordAudio.call(this);}}
          >
            <View
              style={{
                flex:1,
              }}
            >
              <ProgressBar
                ref={(ref)=>this.recordProgressBar= ref}
                styleAttr={'Horizontal'}//android
                progressViewStyle='bar'//ios
                progress={0}
                indeterminate={false}
                style={{
                  transform:[{scaleY:3}],
                }}
              />
            </View>
          </TouchableWithoutFeedback>
          }
          <Icon.Button
            name="plus"
            onPress={() => { this.setState({modalVisible:true});}}
          >
          </Icon.Button>
        </View>
        <Modal
          visible={this.state.modalVisible}
          animationType={'fade'}
          transparent = {true}
          onRequestClose={()=>this.setState({modalVisible:false})}
        >
          <TouchableWithoutFeedback onPress={() => { this.setState({modalVisible:false});}} >
            <View style={{flex:1, flexDirection:'row', justifyContent: 'space-around', alignItems: 'center', backgroundColor:'rgba(0, 0, 0, 0.5)'}} >
              <Icon.Button
                size={width*0.1}
                name="camera"
                onPress={()=>
                  ImagePicker.openCamera({}).then( response=>{
                    return API.uploadFile.call(this,response)
                  }).catch( e=>{
                    Alert.alert('Failure',JSON.stringify(e),[ {text:'OK',},],{cancelable:false})
                  }).finally(()=>{
                    this.setState({modalVisible:false});
                  })
                }
              >
                Camera
              </Icon.Button>
              <Icon.Button
                size={width*0.1}
                name="file-video-o"
                onPress={()=>
                  ImagePicker.openPicker({mediaType:'video'}).then( response=>{
                    return API.uploadFile.call(this,response)
                  }).catch( e=>{
                    Alert.alert('Failure',JSON.stringify(e),[ {text:'OK',},],{cancelable:false})
                  }).finally(()=>{
                    this.setState({modalVisible:false});
                  })
                }
              >
                Video
              </Icon.Button>
              <Icon.Button
                size={width*0.1}
                name="file-image-o"
                onPress={()=> {
                  ImagePicker.openPicker({multiple: true, mediaType:'photo'}).then(response=>{
                    if (Array.isArray(response)) {
                      return Promise.all(response.map((r,i)=>API.uploadFile.call(this,r)))
                    } else {
                      return API.uploadFile.call(this,response)
                    }
                  }).catch( e=>{
                    Alert.alert('Failure',JSON.stringify(e),[ {text:'OK',},],{cancelable:false})
                  }).finally(()=>{
                    this.setState({modalVisible:false});
                  })
                }}
              >
                Photo
              </Icon.Button>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  }

  componentWillUnmount(){
    if(this.audioIndex) {
      let msg = this.state.messageList[this.audioIndex]
      API.stopAudio.call(this,msg);
    }
    this.ws.close();
  }

  _renderItem = (info) => (
    <View
      style={{
        flex:1,
        flexDirection:'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        margin:width * 0.01,
      }}
    >
      <Image
        source={{
          uri: info.item.avatar,
        }}
        style={{
          width: width * 0.1,
          height: width * 0.1,
        }}
        resizeMode='contain'
      />
      <RenderMessage that={this} info={info} styles={{paddingLeft:width * 0.01}}/>
    </View>
  )

  _handleAppStateChange = (appState) =>{
    if ('background' == appState || 'inactive' ==appState) {
      if(this.audioIndex) {
        let msg = this.state.messageList[this.audioIndex]
        API.stopAudio.call(this,msg);
      }
    }
  }
}

function RenderMessage(props){
  const that=props.that
  const msg=props.info.item
  const index=props.info.index
  let ret;
  switch(msg.type) {
    case 1://LectureMessageTypeText
      ret =
      <Text style={[props.styles,{ flex:1, textAlign: 'left', }]} >{msg.content}</Text>
      break;
    case 2://LectureMessageTypeImage
      ret =
      <View style={props.styles}>
        <TouchableWithoutFeedback
          onPress={ () => that.props.navigation.navigate('ViewPager',{
            messageList:that.classifiedMessageList[msg.type],
            type:msg.type,
            initial:msg.content,
          })}
        >
          <Image
            source={{uri:msg.content}}
            style={{
              width: width * 0.3,
              height: width * 0.3,
            }}
            resizeMode='contain'
          />
        </TouchableWithoutFeedback>
      </View>
      break;
    case 3://LectureMessageTypeAudio
      ret =
      <View style={[props.styles,{flexDirection:'row', justifyContent: 'flex-start', alignItems: 'center',}]}>
        <TouchableWithoutFeedback
          onPress={()=>{API.startPlayAudio.call(that,index);}}
        >
          <ProgressBar
            ref={(ref)=>msg.progressBar= ref}
            styleAttr={'Horizontal'}//android
            progressViewStyle='bar'//ios
            progress={0}
            indeterminate={false}
            style={{
              width:width * (msg.duration>60?60:msg.duration) / 75, // max 80% width
              transform:[{scaleY:3}],
            }}
          />
        </TouchableWithoutFeedback>
        <Text>{Math.ceil(msg.duration)}"</Text>
      </View>
      break;
    case 4://LectureMessageTypeVideo
      ret =
      <View style={[props.styles,{flexDirection:'row', justifyContent: 'flex-start', alignItems: 'flex-end',}]}>
        <ImageBackground
          source={{uri: msg.thumbnail || 'http://tva2.sinaimg.cn/crop.0.0.500.500.180/006iN2mkjw8f4rdn5k4gej30dw0dw74w.jpg'}}
          style={[props.styles,{
            width: width * 0.3,
            height: width * 0.3,
            justifyContent:'space-around',
          }]}
          resizeMode='contain'
        >
          <Icon.Button name="play" size={width * 0.1}
            onPress={ () => that.props.navigation.navigate('ViewPager',{
              messageList:that.classifiedMessageList[msg.type],
              type:msg.type,
              initial:msg.content,
            })}>
            PLAY
          </Icon.Button>
        </ImageBackground>
        <Text>{msg.duration || '10s'}</Text>
        <Text> {msg.size || '10MB'}</Text>
      </View>
      break;
    default:
      ret = null;
  }
  return ret;
}

function mapStateToProps(state) {
  return {user:state.user};
}

export default connect( mapStateToProps )(LectureRoom);
