/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
 "use strict";

import React, { Component } from 'react';
import { Button, Text, View } from 'react-native';
import { connect } from 'react-redux';
import * as Basic from './Basic'

class Profile extends React.Component {
  static navigationOptions = ({navigation}) => ({
    title: navigation.state.params?navigation.state.params.name:"Profile",
    header: null,
  });

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View  style={{flex: 1, flexDirection: 'column', justifyContent: 'space-between',}}>
        <RenderSignin that={this}/>
        <View style={{alignItems: 'center',}}>
          <Text>Connect To</Text>
          <Basic.ImageButton
            onPress={() => this.props.navigation.navigate('Oauth',{by:'Weibo'})}
            source={require('../../images/weibo_icon_48x48.png')}
          />
        </View>
        <RenderSignout that={this}/>
      </View>
    );
  }
}

function RenderSignin(props) {
  const that=props.that
  if (that.props.user) {
    return (
      <View>
        <Text>{that.props.user.name}</Text>
      </View>
    );
  } else {
    return (
      <Basic.TextButton
        onPress={() => that.props.navigation.navigate('Signin')}
        title="Signin"
      />
    );
  }
}

function RenderSignout(props) {
  const that=props.that
  if (that.props.user) {
    return (
      <Basic.TextButton
        onPress={() => {
          storage.remove({
            key: 'user'
          });
          that.props.dispatch({type:'USER_SET', user:{}});
        }}
        title="SIGNOUT"
      />
    );
  } else {
    return null;
  }
}

function mapStateToProps(state) {
  return {user:state.user};
}

export default connect( mapStateToProps )(Profile);
