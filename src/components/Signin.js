/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
 "use strict";

import React, { Component } from 'react';
import {StatusBar, TouchableOpacity, KeyboardAvoidingView, Image, Button, View, Text, TextInput, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import Oauth from './Oauth'

class Login extends React.Component {
  static navigationOptions = {
    title: 'Login',
    header: null,
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <KeyboardAvoidingView behavior="padding" style={styles.container}>

        <View style={{padding: 20}}>
          <TextInput style = {styles.input}
            autoCapitalize="none"
            onSubmitEditing={() => this.passwordInput.focus()}
            autoCorrect={false}
            keyboardType='numeric'
            returnKeyType="next"
            placeholder='Mobile Num'
          />
          <TextInput style = {styles.input}
            returnKeyType="go"
            ref={(input)=> this.passwordInput = input}
            keyboardType='numeric'
            placeholder='SMS Code'
            //secureTextEntry
          />
          <TouchableOpacity style={styles.buttonContainer}
            onPress={this.onButtonPress}>
            <Text  style={styles.buttonText}>LOGIN</Text>
          </TouchableOpacity>
        </View>

      </KeyboardAvoidingView>
    );
  }

  onButtonPress = () =>{}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input:{
    height: 40,
    marginBottom: 10,
    padding: 10,
  },
  buttonContainer:{
    paddingVertical: 15
  },
  buttonText:{
    textAlign: 'center',
    fontWeight: '700'
  },
});

function mapStateToProps(state) {
  return {user:state.user};
}

export default connect( mapStateToProps )(Login);
