 "use strict";

import React, { Component } from 'react';
import { View, Text, FlatList, Image, ImageBackground, Dimensions, TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import * as Basic from './Basic'
import * as API from './API';

class LectureList extends React.Component {
  static navigationOptions = {
    title: 'Live',
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      lectureList: [],
    };
    this.count =2;
  }

  render() {
    return (
      <View style={Basic.styles.container}>
        <Basic.TextButton
          onPress={()=>{
            API.getLectureList.call(this).then( lectureList=>{
              this.setState({lectureList:lectureList})
            }).catch( e=>{
              Alert.alert('Failure',JSON.stringify(e),[ {text:'OK',},],{cancelable:false})
            });
          }}
          title={"Click to get " + this.count + " more..."}
        />
        <FlatList
          data={this.state.lectureList}
          renderItem={this._renderItem}
          removeClippedSubviews={true}
          keyExtractor= {(item, i) => i}
          inverted={true}
        />
      </View>
    );
  }

  _renderItem = (info) => (
    <ImageBackground
      source={{
        uri: info.item.coverage,
      }}
      style={{
        width: Dimensions.get('window').width * 0.8,
        height: Dimensions.get('window').width * 0.8 * 8 / 15,
        marginLeft:Dimensions.get('window').width * 0.1,
        marginRight:Dimensions.get('window').width * 0.1,
        marginBottom:Dimensions.get('window').width * 0.01,
      }}
      resizeMode='contain'
    >
      <TouchableOpacity
        style={{
          flex:1 ,
          flexDirection:'column',
          justifyContent:'space-around',
          alignItems:'center',
        }}
        onPress={() => this.props.navigation.navigate('LectureGuide',{id:info.item.id})}
      >
        <Text >{info.item.title}</Text>
      </TouchableOpacity>
    </ImageBackground>
  )
}

export {LectureList as default};
