import {StyleSheet, View, Text, TouchableWithoutFeedback, Image} from 'react-native';
import React, {Component} from 'react';
import {PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator} from 'rn-viewpager';
import PhotoView from 'react-native-photo-view';
import Video from 'react-native-video'; //playVideo

export default class ViewPager extends Component {
  static navigationOptions = {
    title: 'Room',
    header: null,
  };

  constructor(props) {
    super(props);
  }

  render() {
    this.messageList = this.props.navigation.state.params.messageList;
    this.type = this.props.navigation.state.params.type;
    this.initial = this.messageList.indexOf(this.props.navigation.state.params.initial);
    return (
      <IndicatorViewPager
        style={{flex:1}}
        indicator={this._renderDotIndicator()}
        initialPage={this.initial}
      >
        { (2==this.type) &&
          this.messageList.map((item, i) =>
            <View key={i}>
              <PhotoView
                style={{ flex:1 }}
                source={{uri:item}}
                minimumZoomScale={0.5}
                maximumZoomScale={3}
                androidScaleType="center"
                onLoad={() => console.log("Image loaded!")}
                onTap={() => this.props.navigation.goBack()}
              />
            </View>)
        }
        { (4==this.type) &&
          this.messageList.map((item, i) =>
            <View key={i}>
              <Video
                style={{ flex:1 }}
                source={{uri: item}}   // Can be a URL or a local file.
                ref={(ref) => {
                    this.videoPlayer = ref
                }}                                      // Store reference
                rate={1.0}                              // 0 is paused, 1 is normal.
                volume={1.0}                            // 0 is muted, 1 is normal.
                muted={false}                           // Mutes the audio entirely.
                paused={true}                          // Pauses playback entirely.
                resizeMode="cover"                      // Fill the whole screen at aspect ratio.*
                repeat={false}                           // Repeat forever.
                controls={true}
                playInBackground={false}                // Audio continues to play when app entering background.
                playWhenInactive={false}                // [iOS] Video continues to play when control or notification center are shown.
                ignoreSilentSwitch={"ignore"}           // [iOS] ignore | obey - When 'ignore', audio will still play with the iOS hard silent switch set to silent. When 'obey', audio will toggle with the switch. When not specified, will inherit audio settings as usual.
                onLoad={()=>console.log('onLoad')}
                onLoadStart={()=>console.log('onLoadStart')}
                onBuffer={()=>console.log('onBuffer')}
                onReadyForDisplay={()=>console.log('onReadyForDisplay')}
                onEnd={() => this.props.navigation.goBack()}
                disableFocus={false}
              />
            </View>)
        }
      </IndicatorViewPager>
    );
  }

  _renderDotIndicator() {
    return <PagerDotIndicator pageCount={this.messageList.length} />;
  }
}
