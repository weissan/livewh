 "use strict";

import React, { Component } from 'react';
import { View, Text, FlatList, Image, ImageBackground, Dimensions, TouchableOpacity, Alert} from 'react-native';
import { connect } from 'react-redux';
import * as Basic from './Basic'
import * as API from './API';

class LectureGuide extends React.Component {
  static navigationOptions = {
    title: 'Guide',
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      guide: null,
      applying:null,
    };
    this.id = props.navigation.state.params.id;
    API.getLectureGuide.call(this).then(guide=>{
      this.setState({guide:guide});
    }).catch( e=>{
      Alert.alert('Failure',JSON.stringify(e),[ {text:'OK',},],{cancelable:false})
    });
    API.getLectureGuide.call(this).then(applying=>{
      this.setState({applying:applying});
    }).catch( e=>{
      Alert.alert('Failure',JSON.stringify(e),[ {text:'OK',},],{cancelable:false})
    });
  }

  render() {
    return (
      <View  style={Basic.styles.container}>
        <Text>{this.id}</Text>
        <Text>{JSON.stringify(this.state.guide)}</Text>
        <RenderLectureApplying that={this} />
      </View>
        );
  }
}

function RenderLectureApplying(props){
  const that=props.that
  if (!that.state.applying) {
    return (
      <Basic.TextButton
        onPress={()=>{
          API.submitLectureApplying.call(that).then(applying=>{
            this.setState({applying:applying});
          }).catch( e=>{
            Alert.alert('Failure',JSON.stringify(e),[ {text:'OK',},],{cancelable:false})
          });
        }}
        title ='Join Lecture'
      >
      </Basic.TextButton>
    );
  } else {
    return (
      <Basic.TextButton
        onPress={() => that.props.navigation.navigate('LectureRoom',{id:that.id})}
        title='Enter Room'
      >
      </Basic.TextButton>
    );
  }
}

function mapStateToProps(state) {
  return {user:state.user};
}

export default connect( mapStateToProps )(LectureGuide);
