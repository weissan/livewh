/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
 "use strict";

import React, { Component } from 'react';
import { StyleSheet, Text, View, WebView } from 'react-native';
import { connect } from 'react-redux';
import CONFIG from '../config.json';
import * as types from '../redux/actions/Types';

class Oauth extends Component {
  static navigationOptions = ({navigation}) => ({
    title: navigation.state.params.by,
    header: null,
  });

  constructor(props) {
    super(props);
  }

  render () {
    const cfg = CONFIG[this.props.navigation.state.params.by];
    return (
      <View style={[styles.container]}>
        <WebView
          ref={'webview'}
          source={{uri: cfg["authorization_uri"]}}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          onNavigationStateChange={this.onNavigationStateChange.bind(this)}
          onMessage={this.onMessage.bind(this)}
          startInLoadingState={true}
          scalesPageToFit={true}
        />
      </View>
    );
  }

  onNavigationStateChange (navState) {
    console.log(navState);
    const cfg = CONFIG[this.props.navigation.state.params.by];
    if (!navState.loading && navState.url.startsWith(cfg["redirect_uri"])) {
      const script = 'window.postMessage(document.body.innerHTML.replace(/<.+?>/gim,""))';
      this.refs['webview'].injectJavaScript(script);
    }
  }

  onMessage (e) {
    let user=null
    try {
      user = JSON.parse(e.nativeEvent.data);
    } catch(err) {
      console.log(err);
    }
    if (user && user.uid) {
      storage.save({
        key: 'user',
        data: user,
      });
    }
    this.props.dispatch({type:'USER_SET', user:user});
    this.props.navigation.navigate('Profile');
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#F5FCFF',
  },
});

export default connect()(Oauth);
