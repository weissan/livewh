'use strict';

import userReducer from './User';

//return state
function reducers(state, action) {
    return {
        user : userReducer(state.user, action), //slice reducer
    };
}

export {reducers as default};
