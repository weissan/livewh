'use strict';

import {updateObject, updateItemInArray, createReducer} from './Common';

// Slice reducer
const userReducer = createReducer({
  "USER_SET": (state, action)=> action.user,
});

export {userReducer as default};
