'use strict';


import { createStore } from 'redux';
import reducers from '../reducers/Index';

function preload() {
  return {};
}

const store = createStore(reducers, preload());

store.subscribe(function () {
  console.log("In store.subscribe:");
  console.log(store.getState());
});

export {store as default};
