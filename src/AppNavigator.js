"use strict";

import { TabNavigator, StackNavigator } from 'react-navigation';
import Home from './components/Home';
import LectureList from './components/LectureList';
import LectureGuide from './components/LectureGuide';
import LectureRoom from './components/LectureRoom';
import ViewPager from './components/ViewPager';
import Profile from './components/Profile';
import Signin from './components/Signin'
import Oauth from './components/Oauth'

const LectureNavigator  = StackNavigator({
    LectureList: {screen: LectureList},
    LectureGuide: {screen: LectureGuide},
    LectureRoom: {screen: LectureRoom},
    ViewPager: {screen: ViewPager},
  },{
    initialRouteName:'LectureList',
  }
);

const MainNavigator  = TabNavigator({
    Home: {screen: Home},
    LectureNavigator: {screen: LectureNavigator},
    Profile: {screen: Profile},
  },{
    initialRouteName:'Home',
    tabBarPosition: 'bottom',
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: '#00CD00',
      inactiveTintColor: '#000000',
      labelStyle: {
        fontSize: 12,
      },
      style: {
        backgroundColor: 'white',
      },
    },
  }
);

const AppNavigator  = StackNavigator({
    Main: {screen: MainNavigator},
    Signin: {screen: Signin},
    Oauth: {screen: Oauth},
  },{
    initialRouteName:'Main',
  }
);

export {AppNavigator as default};
