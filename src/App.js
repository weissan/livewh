"use strict";

import React, { Component } from 'react';
import { Text, View, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import './Storage';
import AppNavigator from './AppNavigator';

class App extends Component {
  constructor(props) {
    super(props);
    storage.getBatchData([
      { key: 'user', syncInBackground: false, },
    ]).then(ret => {
      props.dispatch({type:'USER_SET', user:ret[0]});
    });
  }

  render() {
    if (typeof this.props.user != "undefined") {
      return(
        <AppNavigator />
      );
    } else {
      return (
        <View style={{flex:1, flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
          <Text style={{textAlign: 'center'}}>Loading...</Text>
          <ActivityIndicator
            animating={true}
            size="large"
          />
        </View>
      );
    }
  }
};

function mapStateToProps(state) {
  return {user:state.user};
}

export default connect( mapStateToProps )(App);
