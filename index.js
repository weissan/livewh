import { AppRegistry } from 'react-native';
import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './src/redux/store/Index';
import App from './src/App';

function AppProvider(props) {
  return(
    <Provider store={store}>
      <App />
    </Provider>
  )
}

AppRegistry.registerComponent('livewh', () => AppProvider);
